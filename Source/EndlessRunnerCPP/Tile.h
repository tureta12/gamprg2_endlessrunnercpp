// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTargetCollidedSignature, class ATile*, Tile); // Declaring the event dispatcher

UCLASS()
class ENDLESSRUNNERCPP_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(BlueprintAssignable) // BluePrintAssignable is exclusive for dynamic multicast delegates 
		FTargetCollidedSignature Exited; // this is the delegate function

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override; // like the construction script in BP?

		int32 noObstacles;
		int32 noPickups;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		int32 minNoObstacles;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		int32 maxNoObstacles;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		int32 minNoPickups;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		int32 maxNoPickups;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		float obstacleSpawnChance; // must be between 0 & 1

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Tile Settings")
		float pickupSpawnChance; // must be between 0 & 1

	UPROPERTY()
		TArray<UChildActorComponent*> childObstacleObjects; // destroy stuff here in deconstruct call maybe

	UPROPERTY()
		TArray<UChildActorComponent*> childPickupObjects;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UArrowComponent* ArrowComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* ExitBoxComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* ObstacleSpawnZone;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		class UBoxComponent* PickupSpawnZone;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FTimerHandle handle;

	// Class reference to Obstacles
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class AObstacle>>ObstacleClass;

	// Class Reference to Pickups
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class APickup>>PickupClass;

	// Collision
	UFUNCTION() // no parameter,the blueprint won't call this anyway
		void OnExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, // begin overlap equivalent
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

		//void OnExitHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintCallable)
		void DespawnTile();	

	UFUNCTION(BlueprintCallable)
		void SpawnObstacle();

	UFUNCTION(BlueprintCallable)
		void SpawnPickup();

	UFUNCTION()
		void SpawnObject(FTransform transformLocation, UClass* object, TArray<UChildActorComponent*> listOfObjects);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
