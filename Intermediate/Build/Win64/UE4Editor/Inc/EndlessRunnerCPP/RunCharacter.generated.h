// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_RunCharacter_generated_h
#error "RunCharacter.generated.h already included, missing '#pragma once' in RunCharacter.h"
#endif
#define ENDLESSRUNNERCPP_RunCharacter_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_9_DELEGATE \
static inline void FPlayerDeathSignature_DelegateWrapper(const FMulticastScriptDelegate& PlayerDeathSignature) \
{ \
	PlayerDeathSignature.ProcessMulticastDelegate<UObject>(NULL); \
}


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDie); \
	DECLARE_FUNCTION(execAddCoin);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDie); \
	DECLARE_FUNCTION(execAddCoin);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_EVENT_PARMS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_CALLBACK_WRAPPERS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpringArm() { return STRUCT_OFFSET(ARunCharacter, SpringArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ARunCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__noCoins() { return STRUCT_OFFSET(ARunCharacter, noCoins); } \
	FORCEINLINE static uint32 __PPO__isAlive() { return STRUCT_OFFSET(ARunCharacter, isAlive); } \
	FORCEINLINE static uint32 __PPO__DeathSound() { return STRUCT_OFFSET(ARunCharacter, DeathSound); } \
	FORCEINLINE static uint32 __PPO__DeathEffect() { return STRUCT_OFFSET(ARunCharacter, DeathEffect); }


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_11_PROLOG \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_EVENT_PARMS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_CALLBACK_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_CALLBACK_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ARunCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
