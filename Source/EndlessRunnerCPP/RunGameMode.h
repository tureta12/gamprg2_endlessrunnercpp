// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNERCPP_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public: 
	ARunGameMode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// some variables
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Game Rules")
		int32 NoInitialTiles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
		FTransform TileSpawnLocation;

	// variable for latest spawned tile
	UPROPERTY(BlueprintReadWrite, Category = "Variables")
		ATile* LatestSpawnedTile;

	// Class reference to the Tile
	UPROPERTY(EditAnywhere)
	class TSubclassOf<class ATile>TileClass; // We just declare it here, but we still have to go inside Unreal BP to assign the class reference

	UFUNCTION(BlueprintCallable)
		void SpawnTile();

	UFUNCTION(BlueprintImplementableEvent)
		void InitializeRunGameModeBP();

	// Event things
	UFUNCTION(BlueprintCallable)
		void OnPlayerExitTile(class ATile* Tile);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnPlayerDeath();
};
