// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_EndlessRunnerCPPGameMode_generated_h
#error "EndlessRunnerCPPGameMode.generated.h already included, missing '#pragma once' in EndlessRunnerCPPGameMode.h"
#endif
#define ENDLESSRUNNERCPP_EndlessRunnerCPPGameMode_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_RPC_WRAPPERS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCPPGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCPPGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), ENDLESSRUNNERCPP_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCPPGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCPPGameMode(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCPPGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), ENDLESSRUNNERCPP_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCPPGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCPPGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunnerCPPGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNERCPP_API, AEndlessRunnerCPPGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCPPGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCPPGameMode(AEndlessRunnerCPPGameMode&&); \
	ENDLESSRUNNERCPP_API AEndlessRunnerCPPGameMode(const AEndlessRunnerCPPGameMode&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ENDLESSRUNNERCPP_API AEndlessRunnerCPPGameMode(AEndlessRunnerCPPGameMode&&); \
	ENDLESSRUNNERCPP_API AEndlessRunnerCPPGameMode(const AEndlessRunnerCPPGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ENDLESSRUNNERCPP_API, AEndlessRunnerCPPGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCPPGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunnerCPPGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_9_PROLOG
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class AEndlessRunnerCPPGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
