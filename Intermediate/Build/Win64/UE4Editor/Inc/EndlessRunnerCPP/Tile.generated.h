// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
struct FTransform;
class UObject;
class UChildActorComponent;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef ENDLESSRUNNERCPP_Tile_generated_h
#error "Tile.generated.h already included, missing '#pragma once' in Tile.h"
#endif
#define ENDLESSRUNNERCPP_Tile_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_9_DELEGATE \
struct _Script_EndlessRunnerCPP_eventTargetCollidedSignature_Parms \
{ \
	ATile* Tile; \
}; \
static inline void FTargetCollidedSignature_DelegateWrapper(const FMulticastScriptDelegate& TargetCollidedSignature, ATile* Tile) \
{ \
	_Script_EndlessRunnerCPP_eventTargetCollidedSignature_Parms Parms; \
	Parms.Tile=Tile; \
	TargetCollidedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnObject); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execDespawnTile); \
	DECLARE_FUNCTION(execOnExitOverlap);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnObject); \
	DECLARE_FUNCTION(execSpawnPickup); \
	DECLARE_FUNCTION(execSpawnObstacle); \
	DECLARE_FUNCTION(execDespawnTile); \
	DECLARE_FUNCTION(execOnExitOverlap);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATile(); \
	friend struct Z_Construct_UClass_ATile_Statics; \
public: \
	DECLARE_CLASS(ATile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ATile)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATile(ATile&&); \
	NO_API ATile(const ATile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATile)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__minNoObstacles() { return STRUCT_OFFSET(ATile, minNoObstacles); } \
	FORCEINLINE static uint32 __PPO__maxNoObstacles() { return STRUCT_OFFSET(ATile, maxNoObstacles); } \
	FORCEINLINE static uint32 __PPO__minNoPickups() { return STRUCT_OFFSET(ATile, minNoPickups); } \
	FORCEINLINE static uint32 __PPO__maxNoPickups() { return STRUCT_OFFSET(ATile, maxNoPickups); } \
	FORCEINLINE static uint32 __PPO__obstacleSpawnChance() { return STRUCT_OFFSET(ATile, obstacleSpawnChance); } \
	FORCEINLINE static uint32 __PPO__pickupSpawnChance() { return STRUCT_OFFSET(ATile, pickupSpawnChance); } \
	FORCEINLINE static uint32 __PPO__childObstacleObjects() { return STRUCT_OFFSET(ATile, childObstacleObjects); } \
	FORCEINLINE static uint32 __PPO__childPickupObjects() { return STRUCT_OFFSET(ATile, childPickupObjects); } \
	FORCEINLINE static uint32 __PPO__SceneComponent() { return STRUCT_OFFSET(ATile, SceneComponent); } \
	FORCEINLINE static uint32 __PPO__ArrowComponent() { return STRUCT_OFFSET(ATile, ArrowComponent); } \
	FORCEINLINE static uint32 __PPO__ExitBoxComponent() { return STRUCT_OFFSET(ATile, ExitBoxComponent); } \
	FORCEINLINE static uint32 __PPO__ObstacleSpawnZone() { return STRUCT_OFFSET(ATile, ObstacleSpawnZone); } \
	FORCEINLINE static uint32 __PPO__PickupSpawnZone() { return STRUCT_OFFSET(ATile, PickupSpawnZone); } \
	FORCEINLINE static uint32 __PPO__handle() { return STRUCT_OFFSET(ATile, handle); } \
	FORCEINLINE static uint32 __PPO__ObstacleClass() { return STRUCT_OFFSET(ATile, ObstacleClass); } \
	FORCEINLINE static uint32 __PPO__PickupClass() { return STRUCT_OFFSET(ATile, PickupClass); }


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_11_PROLOG
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ATile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_Tile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
