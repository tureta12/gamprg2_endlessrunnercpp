// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlessRunnerCPP/EndlessRunnerCPPGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEndlessRunnerCPPGameMode() {}
// Cross Module References
	ENDLESSRUNNERCPP_API UClass* Z_Construct_UClass_AEndlessRunnerCPPGameMode_NoRegister();
	ENDLESSRUNNERCPP_API UClass* Z_Construct_UClass_AEndlessRunnerCPPGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_EndlessRunnerCPP();
// End Cross Module References
	void AEndlessRunnerCPPGameMode::StaticRegisterNativesAEndlessRunnerCPPGameMode()
	{
	}
	UClass* Z_Construct_UClass_AEndlessRunnerCPPGameMode_NoRegister()
	{
		return AEndlessRunnerCPPGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlessRunnerCPP,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "EndlessRunnerCPPGameMode.h" },
		{ "ModuleRelativePath", "EndlessRunnerCPPGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEndlessRunnerCPPGameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::ClassParams = {
		&AEndlessRunnerCPPGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEndlessRunnerCPPGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEndlessRunnerCPPGameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEndlessRunnerCPPGameMode, 4065831183);
	template<> ENDLESSRUNNERCPP_API UClass* StaticClass<AEndlessRunnerCPPGameMode>()
	{
		return AEndlessRunnerCPPGameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEndlessRunnerCPPGameMode(Z_Construct_UClass_AEndlessRunnerCPPGameMode, &AEndlessRunnerCPPGameMode::StaticClass, TEXT("/Script/EndlessRunnerCPP"), TEXT("AEndlessRunnerCPPGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEndlessRunnerCPPGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
