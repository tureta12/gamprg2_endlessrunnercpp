// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Engine/EngineTypes.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Math/Vector.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Scene Component");
	RootComponent = SceneComponent;

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow Component");
	ArrowComponent->SetupAttachment(RootComponent);

	ExitBoxComponent = CreateDefaultSubobject<UBoxComponent>("Exit Box Component");
	ExitBoxComponent->SetupAttachment(RootComponent); 

	ObstacleSpawnZone = CreateDefaultSubobject<UBoxComponent>("Obstacle Spawn Zone Component");
	ObstacleSpawnZone->SetupAttachment(RootComponent);

	PickupSpawnZone = CreateDefaultSubobject<UBoxComponent>("Pickup Spawn Zone Component");
	PickupSpawnZone->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	// Add overlap event
	ExitBoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnExitOverlap);

	// Add Hit event
	//OnActorHit.AddDynamic(this, &ATile::OnExitHit);
}

void ATile::OnConstruction(const FTransform& Transform)
{
	// Spawning Stuff
	if (FMath::RandRange(0.1f, 1.0f) > obstacleSpawnChance)
	{
		noObstacles = FMath::RandRange(minNoObstacles, maxNoObstacles);
		// spawn obstacle
		for (int i = 0; i < noObstacles; i++)
		{
			SpawnObstacle();
		}
	}

	if (FMath::RandRange(0.1f, 1.0f) > pickupSpawnChance)
	{
		noPickups = FMath::RandRange(minNoPickups, maxNoPickups);
		for (int i = 0; i < noPickups; i++)
		{
			// spawn pickup
			SpawnPickup();
		}
	}
}

void ATile::OnExitOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
		// Check if collided with a RunCharacter
	if (AActor* target = Cast<ARunCharacter>(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("Player exited tile"));

		// Call the despawn tile function after some delay (2 seconds)
		GetWorldTimerManager().SetTimer(handle, this, &ATile::DespawnTile, 2.0f);

		// What it does after colliding. It should broadcast the EXITED event with the tile's self (this?) as the parameter.  
		Exited.Broadcast(this); 
	}
}

//void ATile::OnExitHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
//{
//	// Check if collided with a RunCharacter
//	if (AActor* target = Cast<ARunCharacter>(OtherActor))
//	{
//		UE_LOG(LogTemp, Warning, TEXT("Player exited tile"));
//
//		// Call the despawn tile function after some delay (2 seconds)
//		GetWorldTimerManager().SetTimer(handle, this, &ATile::DespawnTile, 2.0f);
//
//		// What it does after colliding. It should broadcast the EXITED event with the tile's self (this?) as the parameter.  
//		OnTargetCollided.Broadcast(this); 
//	}
//}

void ATile::DespawnTile()
{
	UE_LOG(LogTemp, Warning, TEXT("Despawning tile"));
	GetWorldTimerManager().ClearTimer(handle);

	// Destroy the child actors
	if (childObstacleObjects.Num() != NULL && childObstacleObjects.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Ook"));
		for (size_t i = 0; i < childObstacleObjects.Num(); i++)
		{
			childObstacleObjects[i]->DestroyChildActor();
		}
	}

	if (childPickupObjects.Num() != NULL && childPickupObjects.Num() > 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Ook"));
		for (size_t i = 0; i < childPickupObjects.Num(); i++)
		{
			childPickupObjects[i]->DestroyChildActor();
		}
	}

	// Clear or empty the arrays???

	Destroy();
}

void ATile::SpawnObstacle()
{
	FVector randomPoint = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnZone->GetRelativeLocation(), ObstacleSpawnZone->GetScaledBoxExtent());

	FVector obstacleSpawnPoint;
	obstacleSpawnPoint.Set(randomPoint.X, randomPoint.Y, ObstacleSpawnZone->GetRelativeLocation().Z);

	FTransform obstacleTransform;
	obstacleTransform.SetLocation(obstacleSpawnPoint);

	int32 randomInteger = FMath::RandRange(0, ObstacleClass.Num() - 1);
	SpawnObject(obstacleTransform, ObstacleClass[randomInteger], childObstacleObjects);
}

void ATile::SpawnPickup()
{
	FVector randomPoint = UKismetMathLibrary::RandomPointInBoundingBox(PickupSpawnZone->GetRelativeLocation(), PickupSpawnZone->GetScaledBoxExtent());

	FVector pickupSpawnPoint;
	pickupSpawnPoint.Set(randomPoint.X, randomPoint.Y, PickupSpawnZone->GetRelativeLocation().Z);

	FTransform pickupTransform;
	pickupTransform.SetLocation(pickupSpawnPoint);

	int32 randomInteger = FMath::RandRange(0, PickupClass.Num() - 1);
	SpawnObject(pickupTransform, PickupClass[randomInteger], childPickupObjects);
}

void ATile::SpawnObject(FTransform transformLocation, UClass* object, TArray<UChildActorComponent*> listOfObjects)
{
	if (IsValid(object))
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawnin Object"));
		UChildActorComponent* newChildActor = NewObject<UChildActorComponent>(this);
		newChildActor->SetupAttachment(RootComponent);
		newChildActor->RegisterComponent();
		newChildActor->SetRelativeTransform(transformLocation);
		newChildActor->SetChildActorClass(object);
		newChildActor->CreateChildActor();
		listOfObjects.Add(newChildActor);
	}
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

