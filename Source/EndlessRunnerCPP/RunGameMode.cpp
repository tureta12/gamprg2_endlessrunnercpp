// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Math/TransformNonVectorized.h"

ARunGameMode::ARunGameMode()
{

}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Spawn initial tiles
	for (int i = 0; i < NoInitialTiles; i++)
	{
		SpawnTile();
	}

	// temporary work around as the stuff in the BeginPlay of my BP isn't being executed
	InitializeRunGameModeBP();
}

void ARunGameMode::SpawnTile()
{
	// spawns tile
	if (!TileClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Tile Class"));
		return;
	}

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	spawnParams.bNoFail = true;
	spawnParams.Owner = this;

	LatestSpawnedTile = GetWorld()->SpawnActor<ATile>(TileClass, TileSpawnLocation, spawnParams);
	TileSpawnLocation = LatestSpawnedTile->FindComponentByClass<UArrowComponent>()->GetComponentTransform();

	// bind the OnExited custom event to the latest spawned floor tile's Exited event (tell it to listen)
	LatestSpawnedTile->Exited.AddDynamic(this, &ARunGameMode::OnPlayerExitTile);
}

void ARunGameMode::OnPlayerExitTile(ATile* Tile)
{
	ARunGameMode::SpawnTile();
}
