// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacter.h"
#include "RunCharacterController.h"
#include "Sound/SoundWave.h"
#include "Particles/ParticleSystem.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	isAlive = true;
	noCoins = 0;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm Component");
	SpringArm->TargetArmLength = 300.0f;
	SpringArm->SocketOffset.Set(0.0f, 0.0f, 100.0f);
	//SpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	SpringArm->SetupAttachment(RootComponent);
	//SpringArm->SetupAttachment(GetMesh());

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera Component");
	//Camera->AttachToComponent(SpringArm, FAttachmentTransformRules::KeepRelativeTransform); // What's the difference from SetupAttachment(SpringArm)
	Camera->SetupAttachment(SpringArm);
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARunCharacter::AddCoin()
{
	noCoins++;
}

void ARunCharacter::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("Player called to die"));
	// check if alive or not
	if (isAlive == true)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player is alive and thus shall die"));
		isAlive = false;

		//this->DisableInput(GetWorld()->GetFirstPlayerController()); // input hasn't stopped.
		//DisableInput(NULL);
		//this->DisableInput(NULL);
		//ARunCharacter::DisableInput(Cast<ARunCharacterController>(GetController()));
		this->GetController()->UnPossess(); // finally

		GetMesh()->SetVisibility(false);
		OnDeath.Broadcast();
		PlayDeathEffects();
	}
}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

