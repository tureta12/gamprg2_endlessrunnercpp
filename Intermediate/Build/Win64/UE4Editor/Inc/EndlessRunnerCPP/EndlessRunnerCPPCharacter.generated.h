// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_EndlessRunnerCPPCharacter_generated_h
#error "EndlessRunnerCPPCharacter.generated.h already included, missing '#pragma once' in EndlessRunnerCPPCharacter.h"
#endif
#define ENDLESSRUNNERCPP_EndlessRunnerCPPCharacter_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_RPC_WRAPPERS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCPPCharacter(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCPPCharacter_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCPPCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCPPCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunnerCPPCharacter(); \
	friend struct Z_Construct_UClass_AEndlessRunnerCPPCharacter_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunnerCPPCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunnerCPPCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlessRunnerCPPCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunnerCPPCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunnerCPPCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCPPCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunnerCPPCharacter(AEndlessRunnerCPPCharacter&&); \
	NO_API AEndlessRunnerCPPCharacter(const AEndlessRunnerCPPCharacter&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunnerCPPCharacter(AEndlessRunnerCPPCharacter&&); \
	NO_API AEndlessRunnerCPPCharacter(const AEndlessRunnerCPPCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunnerCPPCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunnerCPPCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEndlessRunnerCPPCharacter)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AEndlessRunnerCPPCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AEndlessRunnerCPPCharacter, FollowCamera); }


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_9_PROLOG
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class AEndlessRunnerCPPCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_EndlessRunnerCPPCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
