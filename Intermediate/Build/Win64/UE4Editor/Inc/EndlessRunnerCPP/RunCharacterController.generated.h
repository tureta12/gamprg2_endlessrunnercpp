// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLESSRUNNERCPP_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define ENDLESSRUNNERCPP_RunCharacterController_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_RPC_WRAPPERS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RunCharacter() { return STRUCT_OFFSET(ARunCharacterController, RunCharacter); }


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_10_PROLOG
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ARunCharacterController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
