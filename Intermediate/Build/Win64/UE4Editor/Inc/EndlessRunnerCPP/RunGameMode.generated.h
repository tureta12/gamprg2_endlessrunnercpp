// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ATile;
#ifdef ENDLESSRUNNERCPP_RunGameMode_generated_h
#error "RunGameMode.generated.h already included, missing '#pragma once' in RunGameMode.h"
#endif
#define ENDLESSRUNNERCPP_RunGameMode_generated_h

#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_SPARSE_DATA
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerExitTile); \
	DECLARE_FUNCTION(execSpawnTile);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerExitTile); \
	DECLARE_FUNCTION(execSpawnTile);


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_EVENT_PARMS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_CALLBACK_WRAPPERS
#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesARunGameMode(); \
	friend struct Z_Construct_UClass_ARunGameMode_Statics; \
public: \
	DECLARE_CLASS(ARunGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlessRunnerCPP"), NO_API) \
	DECLARE_SERIALIZER(ARunGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public:


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunGameMode(ARunGameMode&&); \
	NO_API ARunGameMode(const ARunGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunGameMode)


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__NoInitialTiles() { return STRUCT_OFFSET(ARunGameMode, NoInitialTiles); } \
	FORCEINLINE static uint32 __PPO__TileSpawnLocation() { return STRUCT_OFFSET(ARunGameMode, TileSpawnLocation); } \
	FORCEINLINE static uint32 __PPO__LatestSpawnedTile() { return STRUCT_OFFSET(ARunGameMode, LatestSpawnedTile); } \
	FORCEINLINE static uint32 __PPO__TileClass() { return STRUCT_OFFSET(ARunGameMode, TileClass); }


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_12_PROLOG \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_EVENT_PARMS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_RPC_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_CALLBACK_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_INCLASS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_SPARSE_DATA \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_CALLBACK_WRAPPERS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_INCLASS_NO_PURE_DECLS \
	EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ENDLESSRUNNERCPP_API UClass* StaticClass<class ARunGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlessRunnerCPP_Source_EndlessRunnerCPP_RunGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
