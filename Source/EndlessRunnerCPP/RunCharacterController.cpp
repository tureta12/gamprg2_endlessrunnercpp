// Fill out your copyright notice in the Description page of Project Settings.

#include "RunCharacterController.h"
#include "RunCharacter.h"
#include "GameFramework/PlayerController.h"
#include "Components/InputComponent.h"

ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	// get the controlled pawn, then cast it as RunCharacter upon play
	RunCharacter = Cast<ARunCharacter>(ARunCharacterController::GetPawn());
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Automatic Forward movement
	MoveForward(1.0f);
}

void ARunCharacterController::MoveForward(float scale)
{
	//runCharacter->AddInputVector(GetActorForwardVector() * scale);
	RunCharacter->AddMovementInput(RunCharacter->GetActorForwardVector(), scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	//runCharacter->AddInputVector(GetActorRightVector() * scale);
	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector(), scale);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	// Sideward movement
	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}
